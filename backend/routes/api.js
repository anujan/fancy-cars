var express = require('express');
var router = express.Router();

var cars = require('./cars');

/* GET cars listing. */
router.use('/cars', cars);

module.exports = router;
