var express = require('express');
var CarService = require('../services/car');
var AvailabilityService = require('../services/availability');
var router = express.Router();

/* GET cars listing. */
router.get('/', function(req, res, next) {
  CarService.findAll().then(function(cars) {
    Promise.all(cars.map(function(car) {
      return AvailabilityService.find(car.id)
        .then(function(availability) {
          return Object.assign({}, car, availability);
        })
    })).then(function(cars) {
      res.send(cars);
    })
  });
});

module.exports = router;
