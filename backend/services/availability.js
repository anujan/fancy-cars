var axios = require('axios');

var POSSIBLE_VALUES = ['Unavailable', 'In Dealership', 'Out of Stock'];

module.exports = {
  find(id) {
    if (typeof id !== 'number') {
      return new Error('Bad Request. ID must be a number');
    }

    // TODO: Return this instead of the mock
    // return axios.get('/availability?id=' + id);

    return Promise.resolve({
      available: POSSIBLE_VALUES[Math.floor(Math.random() * POSSIBLE_VALUES.length)]
    });
  }
}