var axios = require('axios');
var findAllMock = require('../mocks/cars.json');

module.exports = {
  findAll(limit) {
    if (typeof limit !== 'number') {
      limit = 10;
    }

    // TODO: Return this instead of the mock
    // const apiRequest = axios.get('/cars');

    return Promise.resolve(findAllMock);
  }
}