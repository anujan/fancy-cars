import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import Reboot from 'material-ui/Reboot';

import store from './store';
import HomeContainer from './containers/HomeContainer';
import Header from './components/Header';
import './App.css';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div>
          <Reboot />
          <Header />
          <Route exact path="/" component={HomeContainer} />
        </div>
      </Router>
    </Provider>
  );
}
export default App;
