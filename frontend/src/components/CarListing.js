import React from 'react';
import PropTypes from 'prop-types';
import Grid from 'material-ui/Grid';
import CarItem from './CarItem';

import carShape from '../proptypes/car';

export default function CarListing({ cars }) {
  return (
    <Grid item>
      <h1>Latest Fancy Cars</h1>
      {cars.map((car) => (
        <Grid item xs={12} key={car.id}>
          <CarItem car={car} />
        </Grid>
      ))}
    </Grid>
  );
}

CarListing.propTypes = {
  cars: PropTypes.arrayOf(carShape)
}
