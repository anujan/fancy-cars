import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card, { CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';

import carShape from '../proptypes/car';

const styles = theme => ({
  card: {
    display: 'flex',
    justifyContent: 'space-between',
    '@media screen and (max-width: 960px)': {
      flexDirection: 'column'
    },
    marginBottom: '10px'
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto'
  },
  availability: {

  },
  image: {
    '@media screen and (max-width: 960px)': {
      order: -1,
      width: '100%'
    },
    width: 151,
    height: 151
  }
});

function CarItem(props) {
  const { classes, car } = props;

  return (
    <div>
      <Card className={classes.card}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography variant="headline">{car.name}</Typography>
            <Typography variant="subheading" color="textSecondary">
              {car.make} {car.model} {car.year}
            </Typography>
            <div className={classes.availability}>
              {car.available === "In Dealership" ? (
                <Button size="large" variant="raised" color="primary">
                  BUY
                </Button>
              ) : (
                  <Typography variant="subheading" color="textSecondary">
                    {car.available}
                  </Typography>
                )}
            </div>
          </CardContent>
        </div>
        <CardMedia
          className={classes.image}
          image={car.image}
          title={car.name}
        />
        
      </Card>
    </div>
  );
}

CarItem.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  car: carShape
};

export default withStyles(styles, { withTheme: true })(CarItem);
