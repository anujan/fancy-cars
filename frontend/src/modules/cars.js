const initialState = {
  cars: [],
  loading: true,
  error: false
};

const FETCH_CARS = 'cars/FETCH_CARS';
const LOADED_CARS = 'cars/LOADED_CARS';
const FAILED_LOADING_CARS = 'cars/FAILED_LOADING_CARS';

export const loadCars = (count = 10) => (dispatch, getState) => {
  // TODO: Make middleware that handles this pattern
  dispatch({
    type: FETCH_CARS
  });

  fetch('/api/cars')
    .then((response) => response.json())
    .then((payload) =>
      dispatch({
        type: LOADED_CARS,
        payload 
      })
    ).catch(() =>
      dispatch({
        type: FAILED_LOADING_CARS
      })
    )
}

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_CARS:
      return { ...state, loading: true };
    case LOADED_CARS:
      return { ...state, loading: false, cars: action.payload };
    case FAILED_LOADING_CARS:
      return { ...state, loading: false, error: true };
    default:
      return state;
  }
}
