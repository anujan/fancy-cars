import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Grid from 'material-ui/Grid';
import { CircularProgress } from 'material-ui/Progress';

import { loadCars } from '../modules/cars';
import CarListing from '../components/CarListing';

import carShape from '../proptypes/car';

export class HomeContainer extends Component {
  componentWillMount() {
    this.props.loadCars();
  }

  render() {
    const { cars, loading } = this.props;
    return (
      <main>
        <Grid container spacing={24}>
          <CarListing cars={cars} />
          {loading && <CircularProgress size={100} />}
        </Grid>
      </main>
    );
  }
}

const mapStateToProps = ({ cars: { cars, loading, error } }) => ({
  cars,
  loading,
  error
});

const mapDispatchToProps = (dispatch) => ({
  loadCars: bindActionCreators(loadCars, dispatch)
});

HomeContainer.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.bool,
  cars: PropTypes.arrayOf(carShape)
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
